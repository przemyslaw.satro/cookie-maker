const fs = require("fs");
module.exports = {
  devServer: {
    public: "https://cookie-maker.mac.pl:8080", //do testów
    https: {
      key: fs.readFileSync("./ssl/tls.key"),
      cert: fs.readFileSync("./ssl/tls.crt"),
    },
    allowedHosts: [
      "mac.pl",
      "cookie-maker.mac.pl", // O TO CHODZIŁO!!!!!11
      "cookie-maker.mac.pl:443",
      "cookie-maker.mac.pl:8080",
      "localhost:8080",
    ],
    watchOptions: {
      ignored: /node_modules/,
      aggregateTimeout: 300,
      poll: 1000,
    },
  },
};
