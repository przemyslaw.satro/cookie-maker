import Vue from "vue";
import Vuex from "vuex";
import Cookies from "js-cookie";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cookieBases: [
      {
        name: "light cookie",
        price: 5,
        imgName: "light.png",
        imgPath: "/images/light.png",
        isSelected: true,
      },
      {
        name: "dark cookie",
        price: 6,
        imgName: "dark.png",
        imgPath: "/images/dark.png",
        isSelected: false,
      },
    ],
    cookieAddons: [
      {
        name: "chocolate",
        price: 5,
        imgName: "chocolate.png",
        imgPath: "/images/chocolate.png",
        // isSelected: false,
      },
      {
        name: "sprinkles",
        price: 3,
        imgName: "sprinkles.png",
        imgPath: "/images/sprinkles.png",
        // isSelected: false,
      },
      {
        name: "honey",
        price: 7,
        imgName: "honey.png",
        imgPath: "/images/honey.png",
        // isSelected: false,
      },
      {
        name: "cranberries",
        price: 4,
        imgName: "cranberries.png",
        imgPath: "/images/cranberries.png",
        // isSelected: false,
      },
      {
        name: "coconut",
        price: 10,
        imgName: "coconut.png",
        imgPath: "/images/coconut.png",
        // isSelected: false,
      },
    ],
    currentBase: {
      name: "light cookie",
      price: 5,
      imgName: "light.png",
      imgPath: "/images/light.png",
      isSelected: true,
    },
    currentAddons: [],
    toPay: 0,
  },
  getters: {
    getCookieBases(state) {
      return state.cookieBases;
    },
    getCookieAddons(state) {
      return state.cookieAddons;
    },
    getCurrentBase(state) {
      return state.currentBase;
    },
    getCurrentAddons(state) {
      return state.currentAddons;
    },
  },
  mutations: {
    enabledAllBases(state) {
      state.cookieBases.forEach((el) => {
        el.isSelected = false;
      });
    },
    newCookieBase(state, payload) {
      const index = state.cookieBases.findIndex(
        (el) => el.name === payload.name
      );
      state.cookieBases[index].isSelected = true;
      state.currentBase = payload;
    },
    savedCookieBaseInBrowser(state, payload) {
      Cookies.set("COOKIE_BASE", JSON.stringify(payload), { expires: 2 });
    },
    deleteCookieAddon(state, payload) {
      const iCurrCookieAddons = state.currentAddons.findIndex(
        (el) => el.name === payload.name
      );
      console.log("USUWAM DODATEK!");
      state.currentAddons.splice(iCurrCookieAddons, 1);
    },
    toggleCookieAddon(state, payload) {
      // const iCookieAddons = state.cookieAddons.findIndex(
      //   (el) => el.name === payload.name
      // );
      const iCurrCookieAddons = state.currentAddons.findIndex(
        (el) => el.name === payload.name
      );

      // if (!state.currentAddons.includes(payload)) // zły warunek, z cookiesami były błędy, ale z samym vuexem działało :P
      if (!state.currentAddons.some((el) => el.name === payload.name)) {
        // state.cookieAddons[iCookieAddons].isSelected = true;
        console.log("DODAJE DODATEK!");
        state.currentAddons.push(payload);
      } else {
        // state.cookieAddons[iCookieAddons].isSelected = false;
        console.log("USUWAM DODATEK!");
        state.currentAddons.splice(iCurrCookieAddons, 1);
      }
    },
    savedCookieAddonsInBrowser(state) {
      Cookies.set(
        "COOKIE_ADDONS_SELECTED",
        JSON.stringify(state.currentAddons),
        {
          expires: 2,
        }
      );
    },
    IfCookieBaseFromCookieBrowser(state) {
      const COOKIE_EXIST = !!Cookies.get("COOKIE_BASE");
      if (COOKIE_EXIST) {
        this.commit("enabledAllBases");
        const cookieBaseFromBrowser = JSON.parse(Cookies.get("COOKIE_BASE"));
        state.currentBase = cookieBaseFromBrowser;
        // wyszarza przycisk
        const index = state.cookieBases.findIndex(
          (el) => el.name === cookieBaseFromBrowser.name
        );
        state.cookieBases[index].isSelected = true;
      }
    },
    IfCookieAddonsFromCookieBrowser(state) {
      const ADDONS_EXIST = !!Cookies.get("COOKIE_ADDONS_SELECTED");
      if (ADDONS_EXIST) {
        const cookieAddonsFromBrowser = JSON.parse(
          Cookies.get("COOKIE_ADDONS_SELECTED")
        );
        state.currentAddons = cookieAddonsFromBrowser;
      }
    },
    resetCookies(state) {
      console.log("resetCookies, state: ", state);
      Cookies.set("COOKIE_BASE", "", { expires: 2 });
      Cookies.set("COOKIE_ADDONS_SELECTED", "");
    },
  },
  actions: {},
  modules: {},
});
