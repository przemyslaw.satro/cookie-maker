import Vue from "vue";
import VueRouter from "vue-router";
import CookieShop from "../views/CookieShop.vue";
import Starting from "../views/Starting.vue";
import Register from "../views/Register.vue";
import NotFound from "../views/NotFound.vue";
import Admin from "../views/Admin.vue";
import Keycloak from "keycloak-js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Starting",
    component: Starting,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
  },
  {
    path: "/shop",
    name: "CookieShop",
    component: CookieShop,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/*",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  // store.commit('SET_LOAD_STATE', true);
  // console.log(to, from);

  if (to.name === "CookieShop" || to.name === "Admin") {
    login();
  }
  next();
});
// router.afterEach((to, from) => {
// if(to.name != 'spec-map-view') store.commit('SET_LOAD_STATE', false);
// });

function login() {
  console.log("IM IN!!!!!!!!!!");

  var keycloak = Keycloak({
    realm: `test`,
    url: `https://cookie-keycloak.mac.pl:8082/auth`,
    clientId: `cookie-maker`,
    "ssl-required": "all",
    resource: `cookie-maker`,
    "public-client": true,
    "confidential-port": 0,
  });
  keycloak
    .init({
      // flow: 'implicit',
      promiseType: "native",
      onLoad: "login-required",
    })
    .then((authenticated) => {
      console.log(authenticated);
      // console.log('TOKEN', this.decodeToken(keycloak.token));
      return authenticated;
    })
    .catch((error) => {
      console.log("Błąd");
      console.error(error);
    });
  keycloak.onTokenExpired = () => {
    keycloak
      .updateToken(30)
      .then((refreshed) => {
        if (refreshed) {
          // this.$store.commit("TOKEN_SET", keycloak.token);
        } else {
          console.log("Token is still valid");
        }
      })
      .catch((error) => {
        console.log("CATCH?! ");
        console.error(error);
      });
  };
}

export default router;
