const { MongoClient } = require("mongodb");

let dbConnection;

module.exports = {
  connectToDb: (cb) => {
    console.log("\nConnecting to database...\n");
    MongoClient.connect(
      // lokalnie
      // "mongodb://user:user123@localhost:27017/?authMechanism=DEFAULT"
      // w dockerze
      "mongodb://user:user123@mongodb:27017/?authMechanism=DEFAULT"
    )
      .then((req, res) => {
        // console.log("\n IN CLIENT.JS REQ: ", req);
        // console.log("\n IN CLIENT.JS RES: ", res);
        dbConnection = req.db("cookie-shop");
        console.log("\nConnected to database\n");
        return cb();
      })
      .catch((err) => {
        console.log("ERROR WHILE CONNECTING TO cookie-shop DB!!!! ", err);
        return cb(err);
      });
  },
  getDb: () => {
    // console.log(dbConnection.namespace);
    return dbConnection;
  },
};
//keyclock 11
